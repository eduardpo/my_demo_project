"""test_example.py
"""
import pytest
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import WebDriverException
import chromedriver_binary  # Adds chromedriver binary to path



@pytest.mark.element
def test_should_pass(needle):
    """
    :param NeedleDriver needle: NeedleDriver instance
    :return:
    """
        
    try:
        # Navigate to web page
        needle.driver.get('https://the-internet.herokuapp.com/context_menu')
    except WebDriverException as ex:
        pytest.fail(ex.msg)

    try:
        needle.driver.find_element_by_xpath("//*[contains(text(), 'Right-click in the box below')]")
    except NoSuchElementException as ex:
        pytest.fail(ex.msg)


@pytest.mark.element
def test_should_fail(needle):
    """
    :param NeedleDriver needle: NeedleDriver instance
    :return:
    """

    try:
        # Navigate to web page
        needle.driver.get('https://the-internet.herokuapp.com/context_menu')
    except WebDriverException as ex:
        pytest.fail(ex.msg)
        
    try:
        needle.driver.find_element_by_xpath("//*[contains(text(), 'Alibaba')]")
    except NoSuchElementException as ex:
        pytest.fail(ex.msg)


